### Init config and dependency

```bash
terraform init
```

### Preview config and service for deploy

```bash
terraform plan
```

### Apply config and create service

```bash
terraform apply
```

### Destroy all

```bash
terraform destroy
```

### Add kubeconfig

```bash
aws eks --region ap-southeast-1 update-kubeconfig --name cnx-training-dev-eks
```
