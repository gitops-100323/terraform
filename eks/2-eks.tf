resource "aws_iam_role" "eks-iam-role-cnx-training" {
  name = "eks-cluster-${var.environment}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cnx-training-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-iam-role-cnx-training.name
}

data "aws_region" "current" {
  name = var.AWS_REGION
}

resource "aws_eks_cluster" "cnx-training" {
  name     = var.environment
  role_arn = aws_iam_role.eks-iam-role-cnx-training.arn
  version = "1.25"
  vpc_config {
    subnet_ids = [
      aws_subnet.public_subnet.0.id,
      aws_subnet.public_subnet.1.id,
      aws_subnet.private_subnet.0.id,
      aws_subnet.private_subnet.1.id
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.cnx-training-AmazonEKSClusterPolicy
  ]
}