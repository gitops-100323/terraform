variable "AWS_REGION" {
  default = "ap-southeast-1"
}

variable "emp_id" {
  description = "Employee ID"
  default     = "123456789"
}

variable "environment" {
  description = "Deployment Environment"
  default     = "cnx-training-dev-ec2"
}

variable "vpc_cidr" {
  description = "CIDR block of the vpc"
  default     = "10.94.0.0/16"
}

variable "public_subnets_cidr" {
  description = "CIDR block for Public Subnet"
  default     = ["10.94.3.0/24", "10.94.4.0/24"]

}

variable "private_subnets_cidr" {
  description = "CIDR block for Private Subnet"
  default     = ["10.94.1.0/24", "10.94.2.0/24"]

}

variable "availability_zones" {
  description = "AZ in which all the resources will be deployed"
  default     = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
}
