resource "aws_instance" "ec2-demo" {
  #ami-05b20c6fd82ed6a9b = windows server 2019 base
  #ami-02ee763250491e04a = Ubuntu Server 22.04 LTS (HVM), SSD Volume Type
  ami                         = "ami-02ee763250491e04a"
  instance_type               = "t3.small"
  iam_instance_profile        = "AmazonSSMRoleForInstancesQuickSetup"
  subnet_id                   = aws_subnet.public_subnet.0.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.default.id]
  key_name                    = "key_pair_name"
  source_dest_check           = false

  root_block_device {
    volume_size           = "80"
    volume_type           = "gp2"
    delete_on_termination = true
    encrypted             = true
  }
  tags = {
    Name  = "${var.environment}-ec2"
    EmpID = var.emp_id
  }
}
